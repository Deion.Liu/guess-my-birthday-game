from random import randint

number_of_tries = 1
name = input("Hi! What is your name? ")

for num in range(1, 6):
    month = str(randint(1, 12))
    year = str(randint(1900, 2022))
    print("Guess " + str(num) + ": " + name + " were you born in " + month + "/" + year + "?")
    answer = input("yes or no? ")

    if answer == "yes":
        print("I knew it!")
        break
    elif answer == "no" and num < 5:
        print("Drat! Lemme try again!")
    elif answer == "no" and num == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Bad input!")